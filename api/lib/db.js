const Sequelize = require("sequelize");
/* DATABASE_URL = postgres://login:pass@host:port/db-name */
const connection = new Sequelize("postgres://node:node@localhost:5432/fyc-nuxt-express", {
    /* underscored: true, timestamps: false*/
});

connection
    .authenticate()
    .then(() => console.log("connected to PG"))
    .catch((error) => console.error(error));

module.exports = connection;