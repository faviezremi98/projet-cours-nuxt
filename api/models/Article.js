const connection = require("../lib/db");
const { Model, DataTypes } = require("sequelize");
const User = require('./User');

class Article extends Model {}

Article.init(
    // Schema
    {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                len: {
                    args: [20, 250],
                    msg: 'Votre message doit comprendre entre 20 et 250 caractères'
                }
            }
        },
    },
    // Options
    {
        sequelize: connection,
        modelName: "Article",
    }
);

module.exports = Article;