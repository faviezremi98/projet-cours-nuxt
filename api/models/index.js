const sequelize = require("../lib/db");
const User = require("./User");
const Article = require('./Article');

sequelize
    .sync({ alter: true })
    .then((result) => console.log("Sync OK"))
    .catch((result) => console.error("Sync KO", result));

module.exports = {
    sequelize,
    User,
    Article
}