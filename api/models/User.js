const connection = require("../lib/db");
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcryptjs");
const Article = require("./Article");
class User extends Model {}

User.init(
    // Schema
    {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: { msg: "Email invalid" },
            },
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,

        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                min: 2
            }
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: true,
            validate: {
                isUppercase: { msg: "Votre nom doit être en majuscule" },
                len: [2, 50],
            }
        },
        roles: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    // Options
    {
        sequelize: connection,
        modelName: "User",
        scopes: {
            withoutPassword: {
                attributes: {
                    exclude: ['password']
                }
            }
        }
    }
);


User.addHook("beforeCreate", async(user, options) => {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
});

User.hasMany(Article)
Article.belongsTo(User)

module.exports = User;