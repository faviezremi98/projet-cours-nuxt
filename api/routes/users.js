const { Router } = require('express')
const { User } = require("../models");

const prettifyValidationErrors = (errors) =>
    errors.reduce((acc, field) => {
        acc[field.path] = [...(acc[field.path] || []), field.message];
        return acc;
    }, {});

const { Op, ValidationError } = require("sequelize");


const router = Router()

/* GET users listing. */
router.get('/', function(req, res) {
    User.scope('withoutPassword').findAll().then(
            data => {
                res.status(200).send(data);
            }
        )
        .catch((err) => {
            if (err instanceof ValidationError) {
                res.status(400).json(prettifyValidationErrors(err.message));
            } else {
                res.sendStatus(500);
            }
        });
});

/* GET user by ID. */
router.get('/:id', function(req, res, next) {
    User.findByPk(req.params.id)
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(() => res.sendStatus(500))
});

router.put('/:id', function(req, res, next) {
    console.log(req.body);
    User.update(req.body, { where: { id: req.params.id }, returning: true })
        .then(([countLinesUpdated, result]) => {
            if (countLinesUpdated) {
                res.status(200).json(result[0]);
            } else {
                res.sendStatus(404);
            }
        })
        .catch((err) => {
            if (err.name === "ValidationError") {
                res.status(400).message({ err: prettifyValidationErrors(err.errors) }).send("test");
            } else {
                res.sendStatus(500);
            }
        });
});

router.delete('/:id', function(req, res, next) {
    User.destroy({
            where: {
                id: req.params.id
            }
        })
        .then((countLinesDeleted) => {
            if (countLinesDeleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404)
            }
        })
        .catch(() => res.sendStatus(500));
});

module.exports = router