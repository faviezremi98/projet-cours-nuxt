const { Router } = require('express')
const createToken = require('../lib/authentification').createToken;
const { User } = require("../models");
const router = Router();
const bcrypt = require("bcryptjs");
const { Op, ValidationError } = require("sequelize");

const prettifyValidationErrors = (errors) =>
    typeof errors === "Array" ? errors.reduce((acc, field) => {
        acc[field.path] = [...(acc[field.path] || []), field.message];
        return acc;
    }, {}) : errors;

router.post('/login', (req, res) => {
    const { email, password } = req.body;
    User.findOne({ where: { email } })
        .then(data => {
            if (!data) {
                res.status(401).send({
                    email: "Invalid credentials",
                    password: "Invalid credentials"
                });
            } else {
                bcrypt.compare(password, data.password).then(result => {
                    if (result) {
                        createToken({ email, id: data.id })
                            .then(token => res.json({ data, token }))
                    } else {
                        res.status(401).send({
                            email: "Invalid credentials",
                            password: "Invalid credentials"
                        })
                    }
                })
            }
        })
        .catch((error) => {
            res.status(500).send({ error });
        })
});

router.post("/users", (req, res) => {
    req.body.roles = "ROLE_USER"
    User.create(req.body)
        .then((data) => res.status(201).json(data))
        .catch((err) => {
            if (err instanceof ValidationError) {
                res.status(400).send({ err: prettifyValidationErrors(err.errors) });
            } else {
                res.sendStatus(500);
            }
        });
});

router.post("/admin", (req, res) => {
    req.body.roles = "ROLE_ADMIN"
    User.create(req.body)
        .then((data) => res.status(201).json(data))
        .catch((err) => {
            if (err instanceof ValidationError) {
                res.status(400).send({ err: prettifyValidationErrors(err.errors) });
            } else {
                res.sendStatus(500);
            }
        });
});

module.exports = router