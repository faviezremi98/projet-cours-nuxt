const { Router } = require('express')
const { Article } = require("../models");

const prettifyValidationErrors = (errors) =>
    errors.reduce((acc, field) => {
        acc[field.path] = [...(acc[field.path] || []), field.message];
        return acc;
    }, {});

const { Op, ValidationError } = require("sequelize");


const router = Router()

router.get('/', (req, res) => {
    Article.findAll().then(data => {
        res.status(200).send(data)
    })
    .catch(err => {
        if (err instanceof ValidationError) {
            res.status(400).json(prettifyValidationErrors(err.message));
        } else {
            res.sendStatus(500);
        }
    })
});

router.post('/', (req, res) => {
    req.body.UserId = req.user.id;
    console.log(req.body)
    Article.create(req.body)
        .then(data => {
            res.status(201).send(data)
        })
        .catch((err) => {
            console.log(err)

            if (err instanceof ValidationError) {
                res.status(400).send({ err: prettifyValidationErrors(err.errors) });
            } else {
                res.sendStatus(500);
            }
        });
});

router.get('/get-my-articles', (req, res) => {
    Article.findAll({where: { UserId: req.user.id }})
        .then(data => {
            res.status(200).send(data)
        })
        .catch((err) => {
            if (err instanceof ValidationError) {
                res.status(400).send({ err: prettifyValidationErrors(err.errors) });
            } else {
                res.sendStatus(500);
            }
        });
});

router.get('/:id', (req, res) => {
    Article.findByPk(req.params.id)
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(() => res.sendStatus(500))
});

router.put('/:id', function(req, res, next) {
    Article.update(req.body, { where: { id: req.params.id }, returning: true })
        .then(([countLinesUpdated, result]) => {
            if (countLinesUpdated) {
                res.status(200).json(result[0]);
            } else {
                res.sendStatus(404);
            }
        })
        .catch((err) => {
            if (err.name === "ValidationError") {
                res.status(400).message({ err: prettifyValidationErrors(err.errors) }).send("test");
            } else {
                res.sendStatus(500);
            }
        });
});

router.delete('/:id', (req, res) => {
    Article.destroy({ where: {id: req.params.id} })
        .then((countLinesDeleted) => {
            if (countLinesDeleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404)
            }
        })
        .catch((err) => res.status(500).send(err));
});

module.exports = router