const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

// Create express instance
const app = express()

// Require API routes
const UserRouter = require('./routes/users')
const SecurityRouter = require('./routes/security')
const ArticleRouter = require('./routes/articles')
const verifyToken = require('./middleware/verifyToken')

app.use(cors())
app.use(bodyParser.json())

// Import API Routes
app.use(SecurityRouter)
app.use(verifyToken)
app.use('/articles', ArticleRouter)
app.use('/users', UserRouter)

// Export express app
module.exports = app

// Start standalone server if directly running
if (require.main === module) {
    const port = process.env.PORT || 3001
    app.listen(port, () => {
        // eslint-disable-next-line no-console
        console.log(`API server listening on port ${port}`)
    })
}