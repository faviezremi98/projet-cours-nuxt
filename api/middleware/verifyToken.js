const JWTVerifyToken = require("../lib/authentification").verifyToken;

const verifyToken = (req, res, next) => {
    const authHeader = req.get("Authorization");
    if (!authHeader) { //si y'a pas de Header on renvoit Unauthorized
        return res.sendStatus(401);
    } else if (authHeader.startsWith("Bearer ")) { //Si y'a pas de token Basic on check si y'a un token Bearer
        const token = authHeader.replace("Bearer ", ""); //Utilisé pour le site marchand client
        JWTVerifyToken(token)
            .then((payload) => {
                req.user = payload; //idem on rajoute nos data user à notre requête
                next();
            })
            .catch(() => res.status(401).send({ err: "Unauthorized" }));
    }
};

module.exports = verifyToken;