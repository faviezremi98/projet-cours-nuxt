// VueX Store
export const state = () => ({
  authenticated: false,
  token: '',
  user: ''
})

export const mutations = {
  connect (state, data) {
    state.authenticated = true
    state.token = data.token
    state.user = data.user
  },
  disconnect (state) {
    state.authenticated = false
    state.token = ''
    state.user = ''
  }
}
